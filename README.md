# VtM Animated Scenes for FoundryVTT

A set of animated city and indoor scenes for FoundryVTT taken from the Coteries of New York and Shadows of New York visual novels.

I don't own this stuff. I just wanted to use them in my games. If you like the look of things, you should buy the games. They're fun.
